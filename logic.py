class Logic():
    
    def __init__(self) -> None:
        self._answer = ''
        self._instance = ''
        
    @property
    def answer(self):
        return self._answer

    @answer.setter
    def answer(self, value):
        self._answer = value
        
    @property
    def instance(self):
        return self._instance

    @instance.setter
    def instance(self, value):
        self._instance = value
        
    def calculate(self):
        #В переменную ответ записывается значение функции eval(), в которую передан пример
        self.answer = eval(self.instance)
    
